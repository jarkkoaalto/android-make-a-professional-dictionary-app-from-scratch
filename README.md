# Android - Make a Professional Dictionary App from Scratch

#### About this course

A Complete Project Based Android App Devlopment Course to Build a Professional English Dictionary from Scratch. The Best Way to Learn App Development is Through Making One. And that is exactly what we are going to do in this course.

This course is Completely Project Based. From creating a project in Android Studio to the Complete Application. All in a little over 1 hour. A REAL Life Professional and Play Store Ready App. A Professional and Modern APPLICATION not just something for learning with a sloppy User Interface.

In this course, you will learn how to build a Professional English Dictionary App.

